---
# An instance of the Contact widget.
widget: contact

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 130

title: Contact
subtitle:

content:
  # Automatically link email and phone or display as text?
  autolink: true

  # Email form provider
  form:
    provider: formspree
    formspree:
      id: xrgradjz
    netlify:
      # Enable CAPTCHA challenge to reduce spam?
      captcha: false

  # Contact details (edit or remove options as required)
  email: contact@cody.to
  phone: +1 (561) 614-5612
  address:
    street: 
    city: New Haven
    region: CT
    postcode: '06511'
    country: United States
    country_code: US
  coordinates:
    latitude: '41.30888681735508'
    longitude: '-72.93036210985417'
  directions: 
  office_hours:
    - 'Weekdays 8:00 to 4:30'
  appointment_url: 'https://calendly.com/cody-neiman'
  contact_links:
    - icon: envelope
      icon_pack: fas
      name: Email
      link: mailto:cody-neiman@cody.to
    - icon: cv
      icon_pack: ai
      name: Resume
      link: 'resume'
    - icon: linkedin
      icon_pack: fab
      name: LinkedIn
      link: 'in'
    - icon: github
      icon_pack: fab
      name: GitHub
      link: 'github'
    - icon: project-diagram
      icon_pack: fas
      name: Matrix (Preferred Method)
      link: 'matrix'
    - icon: discord
      icon_pack: fab
      name: Discord
      link: 'discord'
    - icon: whatsapp
      icon_pack: fab
      name: WhatsApp
      link: 'wa'
    - icon: comment
      icon_pack: far
      name: Signal
      link: 'tel:+1%20%28321%29%20525-1054'

design:
  columns: '2'
---
