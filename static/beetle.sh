#!/usr/bin/env bash

sudo pacman -Syyuu --noconfirm
sudo pacman -S --noconfirm base-devel
cd /tmp && git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si --noconfirm && cd /tmp
yay

sudo pacman-key --recv-key FBA220DFC880C036 --keyserver keyserver.ubuntu.com
sudo pacman-key --lsign-key FBA220DFC880C036
sudo pacman -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'
sudo sh -c 'echo "" >> /etc/pacman.conf'
sudo sh -c 'echo "[chaotic-aur]" >> /etc/pacman.conf'
sudo sh -c 'Include = /etc/pacman.d/chaotic-mirrorlist" >> /etc/pacman.conf'
sudo pacman -Syyuu --noconfirm

echo "Set DNS in NetworkManager"
echo "1.1.1.1,1.0.0.0 OR MAL 1.1.1.2,1.0.0.2 OR FAM 1.1.1.3,1.0.0.3"
read

sudo pacman -S --noconfirm firedragon

echo "Set firedragon to default browser"
read

echo "Set DoH in Firedragon"
echo "https://cloudflare-dns.com/dns-query"
read
xdg-open "https://1.1.1.1/help"
read

sudo pacman -S --noconfirm docker docker-compose tigervnc cloudflared

echo "VNC Password: "
read
echo  > ~/.vnc/passwd
printf "$REPLY\n$REPLY\n\n" | vncpasswd
sudo sh -c "echo -e \":1=$USER\" >> /etc/tigervnc/vncserver.users"
sudo cat /etc/tigervnc/vncserver.users

mkdir ~/.vnc
echo -e "session=startplasma-x11\nalwaysshared" >> ~/.vnc/config

sudo systemctl enable docker --now
sudo systemctl enable sshd --now
sudo systemctl enable vncserver@:1.service --now

cloudflared login
mkdir -p ~/.hold/cloudflared/ && cd ~/.hold/cloudflared/
echo -e "version: '3.8'\n\nservices:\n  cloudflared:\n    container_name: 'cloudflared-tunnel'\n    hostname: 'cont-cloudflared'\n    image: 'cloudflare/cloudflared:latest'\n    pull_policy: 'always'\n    network_mode: 'host'\n    volumes:\n      - '/etc/cloudflared/:/etc/cloudflared/'\n    command: 'tunnel --config /etc/cloudflared/config.yml run'\n    user: 'root'\n    restart: 'always'" > ./docker-compose.yml

echo "Tunnel Name: "
read
cloudflared tunnel create $REPLY


sudo mkdir /etc/cloudflared/ ; sudo cp ~/.cloudflared/* /etc/cloudflared/

echo "Tunnel ID: "
read
sudo sh -c "echo -e \"tunnel: $REPLY\" >> /etc/cloudflared/config.yml"
sudo sh -c "echo -e \"credentials-file: /etc/cloudflared/$REPLY.json\" >> /etc/cloudflared/config.yml"

echo "Set CNAME resolver for VNC"
echo "CNAME $REPLY.cfargotunnel.com"
echo "Set CNAME resolver for SSH"
echo "CNAME $REPLY.cfargotunnel.com"
xdg-open "https://dash.cloudflare.com/"
read

echo "VNC URL: "
read
sudo sh -c "echo -e \"ingress:\n- hostname: $REPLY\n  service: tcp://localhost:5901\" >> /etc/cloudflared/config.yml"

echo "Create teams VNC application"
echo "$REPLY"
echo "Include EVERYONE Require EMAILS"
echo "Additional rendering VNC"
xdg-open "https://dash.teams.cloudflare.com/"
read

echo "SSH URL: "
read
sudo sh -c "echo -e \"- hostname: $REPLY\n  service: ssh://localhost:22\n- service: http_status:404\" >> /etc/cloudflared/config.yml"

echo "Create teams SSH application"
echo "$REPLY"
echo "Include EVERYONE Require EMAILS"
echo "Additional rendering SSH"
xdg-open "https://dash.teams.cloudflare.com/"
read

cd ~/.hold/cloudflared/ && sudo docker-compose up -d
