#!/usr/bin/env bash

if [ "$1" == "" ]; then
    echo "A git url must be provided."
    exit 1
else
    echo "Pushing template $2 into $1..."
    git clone $1
    NAME=$(basename "$1" .git)
    cd $NAME
    git switch -c main
    echo "# $NAME" > README.md
    git add .
    git commit -S -m "feat(meta): init repo"
    curl -fsSL https://gitlab.com/Xangelix/gitpod-signed-workspace/-/raw/master/.gitpod.update.sh -o ./.gitpod.update.sh; sudo chmod +x ./.gitpod.update.sh; ./.gitpod.update.sh init "$2"
    git add .
    git commit -S -m "feat(dev): init gitpod env"
    git push -u origin main
    cd ..
    rm -- "$0"
fi
